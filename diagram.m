
filename = 'Players.xlsx';
C = readcell(filename);
[row,col] = size(C);
skill_size = col-1;
player_size = row-1;

skill_names = cell(1,skill_size);
for i = 1:1:skill_size
skill_names{1,i} = C{1,i+1};
end

player_names = cell(1,player_size);
for i = 1:1:player_size
player_names{i,1} = C{i+1,1};
end

skill_point = zeros(player_size,col);

for i = 1:1:player_size
    for j = 1:1:skill_size
        skill_point(i,j) = C{i+1,j+1};
    end
    skill_point(i,col) = C{i+1,2};
end
%%

theta= 0 : 2*pi/skill_size : (2*pi);

selected = [1 2];
% figure('Name',player_names{1,1});
ax = polarplot(theta,skill_point(selected,:),'LineWidth',2);
ax = gca;
ax.ThetaDir = 'clockwise';
ax.ThetaTick = theta*180/pi;
ax.ThetaZeroLocation = 'top';
ax.ThetaTickLabels = skill_names;
ax.ThetaColor = '#A2142F';
ax.FontSize = 12;
legend(player_names{selected,1});

