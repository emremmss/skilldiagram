classdef TUSAS_Championship < matlab.apps.AppBase
    
    % Properties that correspond to app components
    properties (Access = public)
        UIFigure   matlab.ui.Figure
        BaseLabel  matlab.ui.control.Label
        CheckBox   matlab.ui.control.CheckBox
        Button     matlab.ui.control.Button
        Panel      matlab.ui.container.Panel
        var        struct
    end
    
    methods (Access = public)
        function init(app)
            % Dosya okuma ve parametrelere atama
            filename = 'Players.xlsx';
            C = readcell(filename);
            [row,col] = size(C);
            skill_size = col-1;
            player_size = row-1;
            selected = 1;
            
            skill_names = cell(1,skill_size);
            for i = 1:1:skill_size
                skill_names{1,i} = C{1,i+1};
            end
            
            player_names = cell(1,player_size);
            for i = 1:1:player_size
                player_names{i,1} = C{i+1,1};
            end
            
            skill_point = zeros(player_size,col);
            
            for i = 1:1:player_size
                for j = 1:1:skill_size
                    skill_point(i,j) = C{i+1,j+1};
                end
                skill_point(i,col) = C{i+1,2};
            end
            
            app.var = struct;
            
            app.var.row = row;
            app.var.skill_size = skill_size;
            app.var.player_size = player_size;
            app.var.selected = selected;
            app.var.skill_names = skill_names;
            app.var.player_names = player_names;
            app.var.skill_point = skill_point;
            
            pos = struct;
            pos.fig_h = player_size*25+500;
            pos.fig_w = 800;
            app.var.pos = pos;
        end
    end
    
    properties (Access = private)
        Pax % Polar axes
    end
    
    methods (Access = private)
        
        function updateplot(app)
            
            selected = app.var.selected;
            theta= 0 : 2*pi/app.var.skill_size : (2*pi);
            
            % figure('Name',player_names{1,1});
            if ~isempty(selected)
                polarplot(app.Pax,theta,app.var.skill_point(selected,:),'LineWidth',2);
                
                app.Pax.ThetaDir = 'clockwise';
                app.Pax.ThetaTick = theta*180/pi;
                app.Pax.ThetaZeroLocation = 'top';
                app.Pax.ThetaTickLabels = app.var.skill_names;
                app.Pax.ThetaColor = '#A2142F';
                app.Pax.FontSize = 13;
                legend(app.Pax,app.var.player_names{selected,1},'Location','southoutside');
            end
        end
    end
    
    
    % Callbacks that handle component events
    methods (Access = private)
        
        % Code that executes after component creation
        function startupFcn(app)
            
            % Replace 0 in equation with unicode theta
            app.BaseLabel.Text = 'TUSAS League Championship';
            
            % Create polar axes and position it in pixels
            app.Pax = polaraxes(app.UIFigure);
            app.Pax.Units = 'pixels';
            app.Pax.Position = [400 55 300 400];
            
            % Plot the polar function
            updateplot(app);
        end
        
        function ButtonPushed(app, ~)
            selected = zeros(1,app.var.player_size);
            for i = 1:app.var.player_size
                h = get(app.CheckBox(i));
                if h.Value
                    selected(1,i) = i;
                end
            end
            app.var.selected = nonzeros(selected);
            updateplot(app);
        end
    end
    
    % Component initialization
    methods (Access = private)
        
        % Create UIFigure and components
        function createComponents(app)
            fig_h = app.var.pos.fig_h;
            fig_w = app.var.pos.fig_w;
            fig_pc = 550;
            
            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.AutoResizeChildren = 'off';
            app.UIFigure.Position = [100 100 fig_w fig_h];
            app.UIFigure.Name = 'Polar Plot';
            app.UIFigure.Resize = 'off';
            
            % Create BaseLabel
            app.BaseLabel = uilabel(app.UIFigure);
            app.BaseLabel.FontSize = 24;
            app.BaseLabel.HorizontalAlignment = 'center';
            app.BaseLabel.Position = [fig_pc-200 fig_h-50 400 36];
            app.BaseLabel.Text = 'TUSAS Championship';
            
            % Create Button
            app.Button = uibutton(app.UIFigure, 'push');
            app.Button.ButtonPushedFcn = createCallbackFcn(app, @ButtonPushed, true);
            app.Button.Position = [20 80 100 22];
            app.Button.Text = 'Update';
            
            % Create Panel
            panel_h = fig_h - 140;
            app.Panel = uipanel(app.UIFigure);
            app.Panel.AutoResizeChildren = 'off';
            app.Panel.Title = 'Players';
            app.Panel.Scrollable = 'on';
            app.Panel.Position = [20 120 210 panel_h];
            app.Panel.Visible = 'on';
            
            % Create CheckBox
            cbx = zeros(app.var.player_size,1);
            player_names = app.var.player_names;
            for i = 1:app.var.player_size
                cbx(i) = uicheckbox(app.Panel,...
                    'Position',[10 fig_h-i*25 200 22],...
                    'Text',player_names{i,1});
            end
            app.CheckBox = cbx;
            
            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end
    
    % App creation and deletion
    methods (Access = public)
        
        % Construct app
        function app = TUSAS_Championship
            
            % Initializeing
            init(app);
            
            % Create UIFigure and components
            createComponents(app)
            
            % Register the app with App Designer
            registerApp(app, app.UIFigure)
            
            % Execute the startup function
            runStartupFcn(app, @startupFcn)
            
            if nargout == 0
                clear app
            end
        end
        
        % Code that executes before app deletion
        function delete(app)
            
            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end